﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
[RequireComponent(typeof(Image))]
//[RequireComponent(typeof(EventTrigger))]
public class ExtendedToggle : Toggle
{
    public TextMeshProUGUI buttonText;
    public float highlightScaleFactor = 1.1f;
    public bool scaleWhenSelected;
    public Transform[] ignoreScalingObjects;
    public bool animateScaling;
    public float animationDuration = 0.3f;
    //public TooltipUI tooltip;
    public UnityEvent onMouseEnter;
    public UnityEvent onMouseExit;
    public KeyCode alternativeKey;

    public bool changeFontColor;
    public Color defaultFontColor;
    public Color highlightFontColor;
    public Color disabledFontColor;

    public AudioClip mouseDownAudio;
    public AudioClip mouseUpAudio;
    public AudioClip mouseEnterAudio;
    public AudioClip mouseExitAudio;

    private bool isHighlighted;
    private bool isMouseOver;
	//[SerializeField][HideInInspector]
	//private EventTrigger eventTrigger;

#if UNITY_EDITOR
	[MenuItem("GameObject/Custom UI/Extended toggle", false, 10)]
    static void CreateFromMenu(MenuCommand menuCommand)
    {
        // Create a custom game object
        GameObject go = new GameObject("Extended toggle");
        // Ensure it gets reparented if this was a context click (otherwise does nothing)
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
        // Register the creation in the undo system
        Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
        Selection.activeObject = go;
        go.AddComponent<ExtendedToggle>();
        go.GetComponent<ExtendedToggle>().targetGraphic = go.GetComponent<Image>();
        GameObject imageGO = new GameObject("Active image");
        GameObjectUtility.SetParentAndAlign(imageGO, go);
        imageGO.AddComponent<Image>();
        go.GetComponent<ExtendedToggle>().graphic = imageGO.GetComponent<Image>();
        //go.GetComponent<ExtendedToggle>().eventTrigger = go.GetComponent<EventTrigger>();
    }
#endif

    #region Mono behaviour
    protected void Update()
    {
        if (alternativeKey != KeyCode.None && Input.GetKeyDown(alternativeKey)/* && !UIManager.IsPointerOverUI*/)
            isOn = !isOn;
        //OnPointerDown(new PointerEventData(EventSystem.current));
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (isHighlighted && interactable)
        {
            if (LeanTween.isTweening(gameObject))
                LeanTween.cancel(gameObject);
            transform.localScale = new Vector3((highlightScaleFactor + 1f) / 2f, (highlightScaleFactor + 1f) / 2f, 1f);
            //if (UIManager.Instance)
            //    UIManager.Instance.PlayUISound(mouseDownAudio);
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (isHighlighted && interactable)
        {
            if (LeanTween.isTweening(gameObject))
                LeanTween.cancel(gameObject);
            transform.localScale = new Vector3(highlightScaleFactor, highlightScaleFactor, 1f);
            //if (UIManager.Instance)
            //    UIManager.Instance.PlayUISound(mouseUpAudio);
        }
    }

    protected override void Awake()
    {
        base.Awake();
        onValueChanged.AddListener(OnValueChanged);
    }

    private void OnValueChanged(bool active)
    {
        if (!active && scaleWhenSelected && !isMouseOver)
            ToggleHighlight(false);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        ToggleHighlight(false, false);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ToggleHighlight(false, false);
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        isMouseOver = true;
        if (interactable && onMouseEnter.GetPersistentEventCount() > 0 && onMouseEnter != null)
            onMouseEnter.Invoke();
        //if (UIManager.Instance)
        //    UIManager.Instance.PlayUISound(mouseEnterAudio);
        if (interactable && (!isOn || !scaleWhenSelected))
            ToggleHighlight(true, animateScaling);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        isMouseOver = false;
        if (interactable && onMouseExit.GetPersistentEventCount() > 0 && onMouseExit != null)
            onMouseExit.Invoke();
        //if (UIManager.Instance)
        //    UIManager.Instance.PlayUISound(mouseExitAudio);
        if (!isOn || !scaleWhenSelected)
            ToggleHighlight(false, animateScaling);
    }

    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        base.DoStateTransition(state, instant);
        if (state == SelectionState.Disabled)
        {
            if (changeFontColor && buttonText != null)
                buttonText.color = disabledFontColor;
        }
        else if (state == SelectionState.Normal)
        {
            if (changeFontColor && buttonText != null)
                buttonText.color = isHighlighted ? highlightFontColor : defaultFontColor;
        }
    }

    #endregion


    private LTDescr hoverAnimationTween;
    private void ToggleHighlight(bool active, bool playAnimation = true)
    {
        isHighlighted = active;
        if (changeFontColor && buttonText != null)
            buttonText.color = interactable ? (isHighlighted ? highlightFontColor : defaultFontColor) : disabledFontColor;
        if (active)
            HighlightFinished(true);
        //if (!active && tooltip != null)
        //    tooltip.ToggleEnable(false);
        if (playAnimation)
        {
            if (LeanTween.isTweening(gameObject))
                LeanTween.cancel(gameObject);
            hoverAnimationTween = LeanTween.scale(GetComponent<RectTransform>(), active ? new Vector3(highlightScaleFactor, highlightScaleFactor, 1f) : Vector3.one, animationDuration).setEase(LeanTweenType.easeOutExpo).setIgnoreTimeScale(true);
            if (!active)
                hoverAnimationTween.setOnComplete(() => HighlightFinished(false));
        }
        else
        {
            transform.localScale = active ? new Vector3(highlightScaleFactor, highlightScaleFactor, 1f) : Vector3.one;
            if (!active)
                HighlightFinished(false);
        }
    }

    private void HighlightFinished(bool active)
    {
        //if (tooltip != null)
        //{
        //    tooltip.transform.localScale = active ? new Vector3(1f / highlightScaleFactor, 1f / highlightScaleFactor, 1f) : Vector3.one;
        //    tooltip.transform.SetParent(active ? transform.parent : transform, true);
        //    tooltip.ToggleEnable(active);
        //}
        if (ignoreScalingObjects != null && ignoreScalingObjects.Length > 0)
            foreach (var ignoreTransform in ignoreScalingObjects)
                ignoreTransform.SetParent(active ? transform.parent : transform, true);
    }
}

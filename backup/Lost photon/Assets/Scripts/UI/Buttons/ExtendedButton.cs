﻿//using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(Image))]
public class ExtendedButton : Button
{
    public TextMeshProUGUI buttonText;
    //public string textLanguageKey;
    //public string TextLanguageKey
    //{
    //    set
    //    {
    //        textLanguageKey = value;
    //        if (buttonText != null && !string.IsNullOrEmpty(textLanguageKey))
    //            buttonText.text = LocalizationManager.GetTranslation(textLanguageKey);
    //    }
    //}
    public RectTransform targetRect;
    public float highlightScaleFactor = 1.2f;
    public Transform[] ignoreScalingObjects;
    public bool animateScaling = true;
    public Vector3 hoverMovement = Vector3.zero;
    public float animationDuration = 0.3f;
    //public TooltipUI tooltip;
    public bool autoDisplayTooltip = true;
    public UnityEvent onMouseEnter;
    public UnityEvent onMouseExit;
    public KeyCode alternativeKey;
    public bool changeFontColor;
    public Color defaultFontColor;
    public Color highlightFontColor;
    public Color disabledFontColor;

    public AudioClip mouseDownAudio;
    public AudioClip mouseUpAudio;
    public AudioClip mouseEnterAudio;
    public AudioClip mouseExitAudio;

    private CanvasGroup canvasGroup;
    private bool isFaded;
    private bool isHighlighted;
    private bool isMoved;

    public bool IsFaded
    {
        get { return isFaded; }
    }

    public RectTransform TargetRect
    {
        get
        {
            if (targetRect == null)
                targetRect = GetComponent<RectTransform>();
            return targetRect;
        }
    }

#if UNITY_EDITOR
    [MenuItem("GameObject/Custom UI/Extended button", false, 10)]
    static void CreateFromMenu(MenuCommand menuCommand)
    {
        // Create a custom game object
        GameObject go = new GameObject("Extended button");
        // Ensure it gets reparented if this was a context click (otherwise does nothing)
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
        // Register the creation in the undo system
        Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
        Selection.activeObject = go;
        go.AddComponent<ExtendedButton>();
        GameObject textGO = new GameObject("Text");
        GameObjectUtility.SetParentAndAlign(textGO, go);
        textGO.AddComponent<TextMeshProUGUI>();
        go.GetComponent<ExtendedButton>().buttonText = textGO.GetComponent<TextMeshProUGUI>();
    }
#endif

	#region Mono behaviour

	public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (isHighlighted && interactable)
        {
            if (LeanTween.isTweening(TargetRect))
                LeanTween.cancel(TargetRect);
            if (onClick.GetPersistentEventCount() > 0)
                TargetRect.localScale = new Vector3((highlightScaleFactor+1f)/2f, (highlightScaleFactor + 1f) / 2f, 1f);
            //if (UIManager.Instance)
            //    UIManager.Instance.PlayUISound(mouseDownAudio);
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (isHighlighted && interactable)
        {
            if (LeanTween.isTweening(gameObject))
                LeanTween.cancel(gameObject);
            if (onClick.GetPersistentEventCount() > 0)
                TargetRect.localScale = new Vector3(highlightScaleFactor, highlightScaleFactor, 1f);
            //if (UIManager.Instance)
            //    UIManager.Instance.PlayUISound(mouseUpAudio);
        }
    }

    protected void Update()
    {
        if (alternativeKey != KeyCode.None && Input.GetKeyDown(alternativeKey) /*&& !UIManager.IsPointerOverUI*/)
            onClick.Invoke();
            //OnPointerDown(new PointerEventData(EventSystem.current));
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        canvasGroup = GetComponent<CanvasGroup>();
        //if (buttonText != null && !string.IsNullOrEmpty(textLanguageKey))
        //    buttonText.text = LocalizationManager.GetTranslation(textLanguageKey);
        //ToggleHighlight(false, false);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ToggleHighlight(false, false);
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        if (interactable)
        {
            ToggleHighlight(true, animateScaling);
            if (onMouseEnter.GetPersistentEventCount() > 0 && onMouseEnter != null)
                onMouseEnter.Invoke();
        }
        //if(UIManager.Instance)
        //    UIManager.Instance.PlayUISound(mouseEnterAudio);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        if (/*interactable && */onMouseExit.GetPersistentEventCount() > 0 && onMouseExit != null)
            onMouseExit.Invoke();
        //if (UIManager.Instance)
        //    UIManager.Instance.PlayUISound(mouseExitAudio);
        ToggleHighlight(false, animateScaling);
    }

    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        base.DoStateTransition(state, instant);
        if (state == SelectionState.Disabled)
        {
            if (changeFontColor && buttonText != null)
                buttonText.color = disabledFontColor;
        }
        else
        {
            if (changeFontColor && buttonText != null)
                buttonText.color = isHighlighted ? highlightFontColor : defaultFontColor;
        }
    }

    //private void Update()
    //{
    //    if (hoverAnimationTween != null && LeanTween.isTweening(hoverAnimationTween.id))
    //        Debug.Log(transform.localScale + " = " + hoverAnimationTween.ToString());
    //}

    #endregion
    private Vector3 hoverStartPosition;

    private void ToggleHighlight(bool active, bool playAnimation = true)
    {
        isHighlighted = active;
        if (changeFontColor)
            buttonText.color = interactable ? (isHighlighted ? highlightFontColor : defaultFontColor) : disabledFontColor;
        if (active)
            HighlightFinished(true);
        //if (!active && tooltip != null && autoDisplayTooltip)
        //    tooltip.ToggleEnable(false);
        if (playAnimation && LeanTween.isTweening(TargetRect))
                LeanTween.cancel(TargetRect);
        if (playAnimation)
        {
            var hoverAnimationTween = LeanTween.scale(TargetRect, active ? new Vector3(highlightScaleFactor, highlightScaleFactor, 1f) : Vector3.one, animationDuration).setEase(LeanTweenType.easeOutExpo).setIgnoreTimeScale(true);
            if (!active)
                hoverAnimationTween.setOnComplete(() => HighlightFinished(false));
        }
        else
        {
            TargetRect.localScale = active ? new Vector3(highlightScaleFactor, highlightScaleFactor, 1f) : Vector3.one;
            if (!active)
                HighlightFinished(false);
        }
        if (hoverMovement != Vector3.zero && active != isMoved)
        {
            isMoved = active;
            if (active && !LeanTween.isTweening(TargetRect))
                hoverStartPosition = TargetRect.position;
            if (playAnimation)
                LeanTween.move(TargetRect, active ? hoverStartPosition + hoverMovement : hoverStartPosition, animationDuration).setEase(LeanTweenType.easeOutExpo).setIgnoreTimeScale(true);
            else
                TargetRect.anchoredPosition = active ? hoverMovement : Vector3.zero;
        }
    }

    private void HighlightFinished(bool active)
    {
        //if (tooltip != null && autoDisplayTooltip)
        //{
        //    tooltip.transform.localScale = active ? new Vector3(1f / highlightScaleFactor, 1f / highlightScaleFactor, 1f) : Vector3.one;
        //    tooltip.transform.SetParent(active ? transform.parent : transform, true);
        //    tooltip.ToggleEnable(active);
        //}
        //if (ignoreScalingObjects != null && ignoreScalingObjects.Length > 0)
        //    foreach (var ignoreTransform in ignoreScalingObjects)
        //    {
        //        if (active)
        //            UIManager.Instance.HighlightElement(ignoreTransform.gameObject);
        //        else
        //            UIManager.Instance.UnhighlightElement(ignoreTransform.gameObject);
        //    }
    }

    public void ToggleFade(bool active)
    {
        isFaded = active;
        if (canvasGroup != null)
            canvasGroup.alpha = active ? 0.3f : 1f;
    }
}

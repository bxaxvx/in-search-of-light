﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;

    public GamingCardsPanel     GamingCardsPanel;

    [SerializeField]
    private Level _startLevel;
    [SerializeField]
    private ExtendedButton moveLeftButton;
    [SerializeField]
    private ExtendedButton moveRightButton;

    private Level           _currentLevel;
    private CardBase            _selectedCard;

    private void Start()
    {
        _instance = this;
        ChangeLevel(_startLevel);
    }

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void ChangeLevel(Level level)
    {
        if (_currentLevel != null)
            _currentLevel.LeaveLevel();
        _currentLevel = level;
        _currentLevel.EnterLevel();
        if (_currentLevel.nextLevel != null)
        {
            moveRightButton.gameObject.SetActive(true);
            moveRightButton.interactable = !_currentLevel.nextLevel.IsLocked;
        }
        else
            moveRightButton.gameObject.SetActive(false);
        if (_currentLevel.previousLevel != null)
        {
            moveLeftButton.gameObject.SetActive(true);
            moveLeftButton.interactable = !_currentLevel.previousLevel.IsLocked;
        }
        else
            moveLeftButton.gameObject.SetActive(false);
    }

    public void NextLevel()
    {
        Assert.IsNotNull(_currentLevel);
        Assert.IsNotNull(_currentLevel.nextLevel);
        ChangeLevel(_currentLevel.nextLevel);
    }

    public void PrevLevel()
    {
        Assert.IsNotNull(_currentLevel);
        Assert.IsNotNull(_currentLevel.previousLevel);
        ChangeLevel(_currentLevel.previousLevel);
    }

    public void ActivateLevel(string name)
    {
        // TODO: implement
    }

    public void WinGame()
    {
        // TODO: implement
    }

    //public void ApplyCard()
    //{
    //    _currentLevel.ApplyCard(_selectedCard);
    //    _selectedCard = null;
    //}

    public void SelectCard(CardBase card)
    {
       _selectedCard = card;
    }

    public CardBase SelectedCard
    {
        get { return _selectedCard; }
    }

    //public bool CardReadyToAccept
    //{
    //    get { return _currentLevel.CardIsReadyToAccept(SelectedCard); }
    //}

    public void AddCardToPlayer(string cardName)
    {
        CardBase card = Resources.Load<CardBase>(cardName);
        if (card == null)
        {
            Debug.LogErrorFormat("Failed to load {0} card", cardName);
            return;
        }
        GamingCardsPanel.AddCardToSlot(card);
    }
}

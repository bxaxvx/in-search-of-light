﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{
    public enum RobotStateEnum
    {
        StateGood,
        StateBroken
    }

    public enum RobotSystems
    {
        None,
        Microscheme,
        Battery,
        Gear
    }

    public int RobotHitDamage;
    public float CheckAttackDelay;
    public float DamageEnemyDelay;
    public RobotRecoveryController RecoverUI;
    
    public List<string> RandomAttackTriggers;
    public List<RobotSystems> RandomAttackSystems;

    private RobotStateEnum _state = RobotStateEnum.StateGood;
    private HashSet<RobotSystems> _robotSystems = new HashSet<RobotSystems>();
    private RobotSystems _recoveryDetail = RobotSystems.None;
    private Robot _enemy;
    private int _robotHp;

    private IEnumerator _attackCorroutine;
    private Animator _animator;

    private List<RobotSystems> DamageStack = new List<RobotSystems>();

    public RobotStateEnum State
    {
        get { return _state; }
    }

    // Start is called before the first frame update

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void Reset()
    {
        _state = RobotStateEnum.StateGood;
        _robotSystems = new HashSet<RobotSystems>()
        {
            RobotSystems.Microscheme,
            RobotSystems.Gear,
            RobotSystems.Battery
        };
        SetRobotRecoverDetail(RobotSystems.None);
        _enemy = null;
        _robotHp = 100;
        _attackCorroutine = null;
        StopAllCoroutines();
        DamageStack = new List<RobotSystems>();
    }

    //calling from idle animation
    public IEnumerator AttackChecker()
    {
        while (true)
        {
            yield return new WaitForSeconds(CheckAttackDelay);

            if (_enemy &&
                (_robotHp > 0) &&
                (_state == RobotStateEnum.StateGood) &&
                (_enemy._state == RobotStateEnum.StateGood))

            {
                AttackEnemy();
            }
        }
    }

    //calling from hit animation
    public void MakeImpactToEnemy()
    {
        if (_enemy)
        {
            _enemy._animator.SetTrigger("Impact");
            _enemy.ProcessDamagesIfAny();
        }
    }

    public void StartFightWithEnemy(Robot enemy)
    {
        _enemy = enemy;
        if (RobotHitDamage > 0)
        {
            _attackCorroutine = AttackChecker();
            StartCoroutine(_attackCorroutine);
        }
    }

    private void AttackEnemy()
    {
        // add script of attacks here
        int attackIndex = Random.Range(0, RandomAttackTriggers.Count - 1);
        string tirgger = RandomAttackTriggers[attackIndex];
        GetComponent<Animator>().SetTrigger(tirgger);
        _enemy.DamageStack.Add(RandomAttackSystems[attackIndex]);
    }

    public void ProcessDamagesIfAny()
    {
        foreach (var damageSystem in DamageStack)
        {
            MakeDamageToSystem(damageSystem);
        }
        DamageStack.Clear();
    }

    private void MakeDamageToSystem(RobotSystems system)
    {
        _robotSystems.Remove(system);

        if (_recoveryDetail == system)
        {
            _robotSystems.Add(_recoveryDetail);

            // if we are NPC - then we not removing detail
            if (RobotHitDamage == 0)
            {
                if (RecoverUI)
                {
                    RecoverUI.RecoverSystem(_recoveryDetail);
                }
                _recoveryDetail = RobotSystems.None;
            }
        } else
        {
            if (RecoverUI)
            {
                RecoverUI.BreakSystem(system);
            }
        }

        if (RobotHitDamage > 0)
        {
            _robotHp -= RobotHitDamage;
            _robotHp = Mathf.Max(0, _robotHp);
            if (_robotHp == 0)
            {
                Die();
            }
        } else
        {
            UpdateRobotState();
        }
    }

    private void Die()
    {
        // play die animation and add card to player
    }

    public bool HaveRecoveryDetail
    {
        get { return _recoveryDetail != RobotSystems.None; }
    }

    public void SetRobotRecoverDetail(RobotSystems detail)
    {
        if (RecoverUI)
        {
            RecoverUI.SetRecoveryDetail(detail);
        }
        _recoveryDetail = detail;
    }

    private void UpdateRobotState()
    {
        bool allOk =
            _robotSystems.Contains(RobotSystems.Microscheme) &&
            _robotSystems.Contains(RobotSystems.Battery) &&
            _robotSystems.Contains(RobotSystems.Gear);

        if (allOk)
        {
            _state = RobotStateEnum.StateGood;
            _animator.SetBool("Active", true);
        } else
        {
            _state = RobotStateEnum.StateBroken;
            _animator.SetBool("Active", false);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}

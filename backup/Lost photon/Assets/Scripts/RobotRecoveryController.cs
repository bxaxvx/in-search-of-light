﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotRecoveryController : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite Microshceme;
    public Sprite Battery;
    public Sprite Gear;
    public Sprite Blank;
    public Sprite Broken;

    public SpriteRenderer BlankPlace;
    public SpriteRenderer MicroschemePlace;
    public SpriteRenderer BatteryPlace;
    public SpriteRenderer GearPlace;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BreakSystem(Robot.RobotSystems detail)
    {
        switch (detail)
        {
            case Robot.RobotSystems.Microscheme:
                {
                    MicroschemePlace.sprite = Broken;
                    break;
                }
            case Robot.RobotSystems.Battery:
                {
                    BatteryPlace.sprite = Broken;
                    break;
                }
            case Robot.RobotSystems.Gear:
                {
                    GearPlace.sprite = Broken;
                    break;
                }
        }
    }

    public void SetRecoveryDetail(Robot.RobotSystems detail)
    {
        switch (detail)
        {
            case Robot.RobotSystems.Microscheme:
                {
                    BlankPlace.sprite = Microshceme;
                    break;
                }
            case Robot.RobotSystems.Battery:
                {
                    BlankPlace.sprite = Battery;
                    break;
                }
            case Robot.RobotSystems.Gear:
                {
                    BlankPlace.sprite = Gear;
                    break;
                }
            default:
                {
                    BlankPlace.sprite = Blank;
                    break;
                }
        }
    }

    public void RecoverSystem(Robot.RobotSystems detail)
    {
        BlankPlace.sprite = Blank;
        switch (detail)
        {
            case Robot.RobotSystems.Microscheme:
                {
                    MicroschemePlace.sprite = Microshceme;
                    break;
                }
            case Robot.RobotSystems.Battery:
                {
                    BatteryPlace.sprite = Battery;
                    break;
                }
            case Robot.RobotSystems.Gear:
                {
                    GearPlace.sprite = Gear;
                    break;
                }
        }
    }
}

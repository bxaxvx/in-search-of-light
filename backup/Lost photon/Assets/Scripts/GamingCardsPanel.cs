﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GamingCardsPanel : MonoBehaviour
{
    public float SortingDelay = 0.5f;
    public CardSlot CardSlotPrefab;
    // Start is called before the first frame update
    private List<CardSlot> _cardSlots = new List<CardSlot>();
    private Transform _cardsRoot;
    void Start()
    {
        _cardsRoot = GetComponentInParent<Canvas>().transform.Find("CardsRoot");
        StartCoroutine(SortingJob());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator SortingJob()
    {
        while (true)
        {
            yield return new WaitForSeconds(SortingDelay);
            SortCardSlots();
        }
    }

    private void SortCardSlots()
    {
        _cardSlots.Clear();
        for (int i = 0; i < transform.childCount; ++i)
        {
            var cs = transform.GetChild(i).GetComponent<CardSlot>();
            if (cs != null)
            {
                _cardSlots.Add(cs);
            }
        }

        _cardSlots.RemoveAll((CardSlot c) =>
        {
            if (c.HostCard == null)
            {
                GameObject.Destroy(c.gameObject);
                return true;
            }
            return false;
        });
        
    }

    public void AddCardToSlot(CardBase card)
    {
        // TODO: add some sorting here
        var firstSlot = _cardSlots.FirstOrDefault((CardSlot cs) => cs.HostCard == null);
        if (firstSlot == null)
        {
            firstSlot = GameObject.Instantiate<CardSlot>(CardSlotPrefab, this.transform);
        }
        var newCard = GameObject.Instantiate<CardBase>(card, _cardsRoot);
        firstSlot.PlaceCard(newCard);
    }
}

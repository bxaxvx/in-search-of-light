﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLocation : Location
{
    public override void ApplyCard(CardBase card)
    {
        card.StartDestroying();
    }
}

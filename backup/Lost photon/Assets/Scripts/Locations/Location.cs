using UnityEngine;

public abstract class Location : MonoBehaviour
{
    [SerializeField]
    protected SpriteRenderer backgroundRenderer;

    public abstract void ApplyCard(CardBase card);
    //public virtual bool CardIsReadyToAccept(CardBase card) { return card != null; }


}
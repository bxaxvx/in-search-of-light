﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CardBase 
    : MonoBehaviour
    , IPointerEnterHandler
    , IPointerExitHandler
    , IBeginDragHandler
    , IDragHandler
    , IEndDragHandler

{
    private CardSlot _attachedTo = null;
    private bool _selected = false;
    private bool _dragging = false;
    private bool _readyToAccept = false;


    public void AttachToSlot(CardSlot slot)
    {
        if (_attachedTo)
        {
            DetachFromSlot();
        }
        _attachedTo = slot;
    }

    private void DetachFromSlot()
    {
        _attachedTo.HostCard = null;
        _attachedTo = null;
    }

    public void StartDestroying()
    {
        // TODO: can add animation here
        DetachFromSlot();
        Destroy(gameObject, 0.3f);
    }

    public void ReadyToAccept(bool ready)
    {
        _readyToAccept = ready;
    }

    public bool IsSelected
    {
        get
        {
            return _selected;
        }
    }

    public bool IsDragging
    {
        get
        {
            return _dragging;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _selected = true;
        // sorting in order to be on top of other cards
        int maxIndex = transform.parent.childCount;
        transform.SetSiblingIndex(maxIndex - 1);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _selected = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _dragging = true;
        GameManager.Instance.SelectCard(this);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _dragging = false;
        //if (_readyToAccept)
        //{
        //    GameManager.Instance.ApplyCard();
        //}
    }
}

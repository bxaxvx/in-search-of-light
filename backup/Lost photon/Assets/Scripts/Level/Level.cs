﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Level previousLevel;
    public Level nextLevel;

    [SerializeField]
    private List<Location> locations;
    [SerializeField]
    private bool isLocked;

    public bool IsLocked
    {
        get { return isLocked; }
    }

    private bool wasVisitedBefore;

    public void EnterLevel()
    {
        wasVisitedBefore = true;
        gameObject.SetActive(true);
    }

    public void LeaveLevel()
    {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CardSlot : MonoBehaviour
{
    public List<float> RandomCardRotations;
    public CardBase HostCard;
    public Vector3 SelectedOffset;
    public float AnimationSpeed = 0.1f;

    private Quaternion MyRotation;

    // Start is called before the first frame update
    void Start()
    {
        MyRotation = Quaternion.Euler(0, 0, RandomCardRotations[Random.Range(0, RandomCardRotations.Count)]);
    }

    // Update is called once per frame
    void Update()
    {
        // TODO: some simlpe animation here
        if (HostCard && !HostCard.IsDragging)
        {
            if (HostCard.IsSelected)
            {
                HostCard.transform.position = Vector3.Lerp(HostCard.transform.position, transform.position + SelectedOffset, AnimationSpeed);
                HostCard.transform.rotation = Quaternion.Lerp(HostCard.transform.rotation, Quaternion.identity, AnimationSpeed);
            } else
            {
                HostCard.transform.position = Vector3.Lerp(HostCard.transform.position, transform.position, AnimationSpeed);
                HostCard.transform.rotation = Quaternion.Lerp(HostCard.transform.rotation, MyRotation, AnimationSpeed);
            }
        }
    }

    public void PlaceCard(CardBase card)
    {
        // TODO: can place animation here
        HostCard = card;
        card.AttachToSlot(this);
    }
}

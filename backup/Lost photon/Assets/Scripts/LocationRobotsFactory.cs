﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationRobotsFactory : Location
{
    public FightController FightController;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnEnable()
    {
        GameManager.Instance.AddCardToPlayer("GamingCardRobotBattery");
        GameManager.Instance.AddCardToPlayer("GamingCardRobotGear");
        GameManager.Instance.AddCardToPlayer("GamingCardRobotMicroscheme");
        FightController.StartFight();
    }

    public override void ApplyCard(CardBase card)
    {
        card.StartDestroying();
    }
}

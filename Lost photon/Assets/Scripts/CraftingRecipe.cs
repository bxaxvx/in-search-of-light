﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CraftingRecipe 
{
    public ECardType cardA;
    public ECardType cardB;
    public ECardType resultCard;
}

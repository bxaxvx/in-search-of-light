﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;

public class CardDisplay : MonoBehaviour
    , IPointerEnterHandler
    , IPointerExitHandler
    , IBeginDragHandler
    , IDragHandler
    , IEndDragHandler
{
    private const float HOVER_SCALE_FACTOR = 1.1f;

    [SerializeField]
    private AudioClip hoverSound;
    [SerializeField]
    private Image iconImage;
    [SerializeField]
    private TextMeshProUGUI placeholderText;
    private CardInfo cardInfo;
    public CardInfo CardInfo
    {
        get { return cardInfo; }
    }
    private CardSlot _attachedTo = null;
    private CardBoard _hoverBoard = null;

    private bool _selected = false;
    private bool _dragging = false;

    public void Init(CardInfo cardInfo)
    {
        this.cardInfo = cardInfo;
        if (cardInfo.cardSprite != null)
            iconImage.sprite = cardInfo.cardSprite;
        else
            placeholderText.text = cardInfo.placeholderName;
    }

    public void HoverBoard(CardBoard board)
    {
        _hoverBoard = board;
    }

    public void AttachToSlot(CardSlot slot)
    {
        if (_attachedTo)
        {
            DetachFromSlot();
        }
        _attachedTo = slot;
    }

    private void DetachFromSlot()
    {
        _attachedTo.hostedCard = null;
        _attachedTo = null;
    }

    public void StartDestroying()
    {
        // TODO: can add animation here
        DetachFromSlot();
        Destroy(gameObject, 0.3f);
    }

    public bool IsSelected
    {
        get
        {
            return _selected;
        }
    }

    public bool IsDragging
    {
        get
        {
            return _dragging;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = new Vector3(HOVER_SCALE_FACTOR, HOVER_SCALE_FACTOR, 1f);
        _selected = true;
        // sorting in order to be on top of other cards
        int maxIndex = transform.parent.childCount;
        transform.SetSiblingIndex(maxIndex - 1);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = Vector3.one;
        _selected = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        GameManager.Instance.PlaySoundFX(hoverSound, 0.5f, true);
        _dragging = true;
        _hoverBoard = null;
        GameManager.Instance.SelectCard(this);
        GameManager.Instance.craftManager.OnCardPickedFromSlot(this);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
        _hoverBoard = null;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GameManager.Instance.PlaySoundFX(hoverSound, 0.5f, true);
        _dragging = false;
        if (_hoverBoard != null)
        {
            var slot = _hoverBoard.GetComponent<CardSlot>();
            if (slot != null)
            {
                slot.PlaceCard(this);
            }

            var cardsPanel = _hoverBoard.GetComponent<GamingCardsPanel>();
            if (cardsPanel)
            {
                cardsPanel.AddCardToSlot(this);
            }

            var craftSlot = _hoverBoard.GetComponent<CraftSlot>();
            if (craftSlot)
            {
                craftSlot.OnDragEnd(this);
            }
        }
    }
}

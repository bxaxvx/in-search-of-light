﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CraftManager : MonoBehaviour
{
    public AudioClip craftSound;
    public AudioClip disassembleSound;

    [SerializeField]
    private CardSlot[] craftSlots;
    [SerializeField]
    private CardSlot[] disassembleSlots;
    [SerializeField]
    private CardDisplay CardDisplayPrefab;
    [SerializeField]
    private Transform CardsRoot;

    [SerializeField]
    private GameObject craftHolder;
    [SerializeField]
    private GameObject disassemblerHolder;

    private bool isInCraftingMode = true;
    private bool craftCompleted = false;

    public void OnModeChanged(bool isCraftingMode)
    {
        if (craftSlots[0].hostedCard != null)
            GameManager.Instance.GamingCardsPanel.AddCardToSlot(craftSlots[0].hostedCard);
        if (craftSlots[1].hostedCard != null)
            GameManager.Instance.GamingCardsPanel.AddCardToSlot(craftSlots[1].hostedCard);
        if (craftSlots[2].hostedCard != null)
            craftSlots[2].hostedCard.StartDestroying();
        if (disassembleSlots[0].hostedCard != null)
            GameManager.Instance.GamingCardsPanel.AddCardToSlot(disassembleSlots[0].hostedCard);
        else
        {
            if (disassembleSlots[1].hostedCard != null)
                GameManager.Instance.GamingCardsPanel.AddCardToSlot(disassembleSlots[1].hostedCard);
            if (disassembleSlots[2].hostedCard != null)
                GameManager.Instance.GamingCardsPanel.AddCardToSlot(disassembleSlots[2].hostedCard);
        }
        if (disassembleSlots[1].hostedCard != null)
            disassembleSlots[1].hostedCard.StartDestroying();
        if (disassembleSlots[2].hostedCard != null)
            disassembleSlots[2].hostedCard.StartDestroying();
        craftCompleted = false;
        isInCraftingMode = isCraftingMode;
        craftHolder.SetActive(isCraftingMode);
        disassemblerHolder.SetActive(!isCraftingMode);
    }

    public void OnCardAddedToSlot()
    {
        if (isInCraftingMode)
        {
            if (craftSlots[0].hostedCard == null || craftSlots[1].hostedCard == null)
                return;
            ECardType outputCard = GameManager.Instance.GetCraftResult(craftSlots[0].hostedCard.CardInfo, craftSlots[1].hostedCard.CardInfo);
            if (outputCard == ECardType.None)
                return;
            craftCompleted = true;
            var newCard = Instantiate(CardDisplayPrefab, CardsRoot);
            newCard.Init(GameManager.Instance.GetCardInfo(outputCard));
            craftSlots[2].PlaceCard(newCard);
        }
        else
        {
            if (disassembleSlots[0].hostedCard == null)
                return;
            var outputCards = GameManager.Instance.GetDisassembleResult(disassembleSlots[0].hostedCard.CardInfo);
            if (outputCards == null)
                return;
            craftCompleted = true;
            var newCard = Instantiate(CardDisplayPrefab, CardsRoot);
            newCard.Init(GameManager.Instance.GetCardInfo(outputCards[0]));
            disassembleSlots[1].PlaceCard(newCard);
            if (outputCards.Length == 2)
            {
                newCard = Instantiate(CardDisplayPrefab, CardsRoot);
                newCard.Init(GameManager.Instance.GetCardInfo(outputCards[1]));
                disassembleSlots[2].PlaceCard(newCard);
            }
        }
    }

    public void OnCardPickedFromSlot(CardDisplay cardDisplay)
    {
        if (craftCompleted)
        {
            if (isInCraftingMode)
            {
                Assert.IsNotNull(cardDisplay);
                if (craftSlots[2].hostedCard == cardDisplay)
                {
                    GameManager.Instance.PlaySoundFX(craftSound, 1f, true);
                    craftSlots[1].hostedCard.StartDestroying();
                    craftSlots[0].hostedCard.StartDestroying();
                }
                else
                {
                    craftSlots[2].hostedCard.StartDestroying();
                }
                craftCompleted = false;
            }
            else
            {
                Assert.IsNotNull(cardDisplay);
                if (disassembleSlots[0].hostedCard == cardDisplay )
                {
                    disassembleSlots[1].hostedCard.StartDestroying();
                    disassembleSlots[2].hostedCard.StartDestroying();
                }
                else
                {
                    disassembleSlots[0].hostedCard.StartDestroying();
                    GameManager.Instance.PlaySoundFX(disassembleSound, 1f, true);
                }
                craftCompleted = false;
            }
        }
    }
}

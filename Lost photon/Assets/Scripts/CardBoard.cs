﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardBoard : MonoBehaviour
{
    public CardDisplay HoverCard;
    public RectTransform _rectTransform;
    // Update is called once per frame

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        HoverCard = null;
        // for some reason unity event system behaves strange
        // just calculate if screenpoint in rectangle area
        if (GameManager.Instance.SelectedCard &&
            RectTransformUtility.RectangleContainsScreenPoint(_rectTransform, Input.mousePosition))
        {
            HoverCard = GameManager.Instance.SelectedCard;
        }
    }

    private void LateUpdate()
    {
        if (HoverCard != null)
        {
            HoverCard.HoverBoard(this);
        }
    }
}

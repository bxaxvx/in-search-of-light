﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameAnimation : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private Sprite[] sprites;
    [SerializeField]
    private float framesPerSecond;
    private bool isPlayingLoop;
    private bool isPlayingSingle;
    private int animationDuration;

    private int offset;
    private float playingTime;

    private void Awake()
    {
        PlayLoop(spriteRenderer, sprites, framesPerSecond);
    }
    public void PlayLoop(SpriteRenderer spriteRenderer, Sprite[] sprites, float speed = 0f)
    {
        if (sprites == null || sprites.Length == 0)
            return;
        if (!isPlayingLoop)
            offset = Random.Range(0, sprites.Length);

        this.spriteRenderer = spriteRenderer;
        this.sprites = sprites;
        framesPerSecond = speed;
        isPlayingSingle = false;
        if (sprites.Length == 1 || speed == 0f)
        {
            isPlayingLoop = false;
            spriteRenderer.sprite = sprites[0];
        }
        else
        {
            if (framesPerSecond > 0)
                animationDuration = (int)(((float)sprites.Length / framesPerSecond) * 1000f);
            isPlayingLoop = sprites.Length > 1 && speed > 0f;
        }
    }

    public void StopLoop()
    {
        isPlayingLoop = false;
        isPlayingSingle = false;
    }

    public void PlaySingle(SpriteRenderer spriteRenderer, Sprite[] sprites, float speed)
    {
        this.spriteRenderer = spriteRenderer;
        this.sprites = sprites;
        framesPerSecond = speed;
        isPlayingLoop = false;
        playingTime = 0f;
        if (framesPerSecond > 0)
            animationDuration = (int)(((float)sprites.Length / framesPerSecond) * 1000f);
        isPlayingSingle = sprites.Length > 1 && framesPerSecond > 0f;
    }

    private void Update()
    {
        if (isPlayingLoop)
            spriteRenderer.sprite = sprites[((int)(((Time.time * 1000f) % animationDuration) / (animationDuration / sprites.Length)) + offset) % sprites.Length];
        else if (isPlayingSingle)
        {
            if (playingTime > animationDuration)
            {
                isPlayingSingle = false;
                spriteRenderer.sprite = null;
            }
            else
            {
                spriteRenderer.sprite = sprites[(int)((playingTime % animationDuration) / (animationDuration / sprites.Length))];
                playingTime += Time.deltaTime * 1000f;
            }
        }
    }
}


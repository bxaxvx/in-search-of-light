﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CardSlot : MonoBehaviour
{
    public List<float> RandomCardRotations;
    public CardDisplay hostedCard;
    public Vector3 SelectedOffset;
    public float AnimationSpeed = 0.1f;

    private Quaternion MyRotation;

    // Start is called before the first frame update
    void Start()
    {
        MyRotation = Quaternion.Euler(0, 0, RandomCardRotations[Random.Range(0, RandomCardRotations.Count)]);
    }

    // Update is called once per frame
    void Update()
    {
        // TODO: some simlpe animation here
        if (hostedCard && !hostedCard.IsDragging)
        {
            if (hostedCard.IsSelected)
            {
                hostedCard.transform.position = Vector3.Lerp(hostedCard.transform.position, transform.position + SelectedOffset, AnimationSpeed);
                hostedCard.transform.rotation = Quaternion.Lerp(hostedCard.transform.rotation, Quaternion.identity, AnimationSpeed);
            } else
            {
                hostedCard.transform.position = Vector3.Lerp(hostedCard.transform.position, transform.position, AnimationSpeed);
                hostedCard.transform.rotation = Quaternion.Lerp(hostedCard.transform.rotation, MyRotation, AnimationSpeed);
            }
        }
    }

    public void PlaceCard(CardDisplay card)
    {
        if (hostedCard)
        {
            if (hostedCard == card)
            {
                return;
            }
            GameManager.Instance.GamingCardsPanel.AddCardToSlot(hostedCard);
            hostedCard = null;
        }
        //card.transform.SetParent(transform, true);
        //card.transform.localScale = Vector3.one;
        hostedCard = card;
        card.AttachToSlot(this);
    }
}

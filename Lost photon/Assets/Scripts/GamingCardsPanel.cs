﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GamingCardsPanel : MonoBehaviour
{
    public float SortingDelay = 0.5f;
    public CardSlot CardSlotPrefab;
    public CardDisplay CardDisplayPrefab;

    // Start is called before the first frame update
    private List<CardSlot> _cardSlots = new List<CardSlot>();
    [SerializeField]
    private Transform _cardsRoot;

    void OnEnable()
    {
        StartCoroutine(SortingJob());
    }

    private IEnumerator SortingJob()
    {
        while (true)
        {
            yield return new WaitForSeconds(SortingDelay);
            SortCardSlots();
        }
    }

    private void SortCardSlots()
    {
        _cardSlots.Clear();
        for (int i = 0; i < transform.childCount; ++i)
        {
            var cs = transform.GetChild(i).GetComponent<CardSlot>();
            if (cs != null)
            {
                _cardSlots.Add(cs);
            }
        }

        _cardSlots.RemoveAll((CardSlot c) =>
        {
            if (c.hostedCard == null)
            {
                GameObject.Destroy(c.gameObject);
                return true;
            }
            return false;
        });        
    }

    public void AddCardToSlot(CardInfo cardInfo)
    {
        var newCard = Instantiate(CardDisplayPrefab, _cardsRoot);
        newCard.Init(cardInfo);
        AddCardToSlot(newCard);
    }

    public void AddCardToSlot(CardDisplay cardDisplay)
    {
        var firstSlot = _cardSlots.FirstOrDefault((CardSlot cs) => cs.hostedCard == null);
        if (firstSlot == null)
            firstSlot = Instantiate(CardSlotPrefab, transform);
        firstSlot.PlaceCard(cardDisplay);
    }
}

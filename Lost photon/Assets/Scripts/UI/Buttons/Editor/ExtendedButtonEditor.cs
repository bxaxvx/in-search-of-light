﻿using TMPro;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Events;

[AddComponentMenu("UI/Extended Button", 30)]
[CanEditMultipleObjects]
[CustomEditor(typeof(ExtendedButton))]
public class ExtendedButtonEditor : UnityEditor.UI.ButtonEditor
{
    private SerializedProperty onMouseEnterProperty;
    private SerializedProperty onMouseExitProperty;
    private SerializedProperty ignoreScalingObjectsProperty;
    private SerializedProperty mouseDownAudio;
    private SerializedProperty mouseUpAudio;
    private SerializedProperty mouseEnterAudio;
    private SerializedProperty mouseExitAudio;
    private SerializedProperty hoverMovement;
    private SerializedProperty targetRect;
    private SerializedProperty alternativeKey;

    private SerializedProperty changeFontColor;
    private SerializedProperty defaultFontColor;
    private SerializedProperty highlightFontColor;
    private SerializedProperty disabledFontColor;

    protected override void OnEnable()
    {
        base.OnEnable();
        onMouseEnterProperty = serializedObject.FindProperty("onMouseEnter");
        onMouseExitProperty = serializedObject.FindProperty("onMouseExit");
        ignoreScalingObjectsProperty = serializedObject.FindProperty("ignoreScalingObjects");

        mouseDownAudio = serializedObject.FindProperty("mouseDownAudio");
        mouseUpAudio = serializedObject.FindProperty("mouseUpAudio");
        mouseEnterAudio = serializedObject.FindProperty("mouseEnterAudio");
        mouseExitAudio = serializedObject.FindProperty("mouseExitAudio");
        targetRect = serializedObject.FindProperty("targetRect");
        hoverMovement = serializedObject.FindProperty("hoverMovement");
        alternativeKey = serializedObject.FindProperty("alternativeKey");

        changeFontColor = serializedObject.FindProperty("changeFontColor");
        defaultFontColor = serializedObject.FindProperty("defaultFontColor");
        highlightFontColor = serializedObject.FindProperty("highlightFontColor");
        disabledFontColor = serializedObject.FindProperty("disabledFontColor");

    }

    public override void OnInspectorGUI()
    {
        ExtendedButton component = (ExtendedButton)target;
        base.OnInspectorGUI();
        component.buttonText = (TextMeshProUGUI)EditorGUILayout.ObjectField("Text field", component.buttonText, typeof(TextMeshProUGUI), true);
        //component.textLanguageKey = EditorGUILayout.TextField("Text language key", component.textLanguageKey);
        EditorGUILayout.PropertyField(targetRect, true);
        component.highlightScaleFactor = EditorGUILayout.FloatField("Hover scale factor", component.highlightScaleFactor);
        component.animateScaling = EditorGUILayout.Toggle("Animate hover scaling", component.animateScaling);
        EditorGUILayout.PropertyField(hoverMovement, true);
        component.animationDuration = EditorGUILayout.FloatField("Hover animation duration", component.animationDuration);
        EditorGUILayout.PropertyField(ignoreScalingObjectsProperty, true);
        //component.tooltip = (TooltipUI)EditorGUILayout.ObjectField("Tooltip", component.tooltip, typeof(TooltipUI), true);
        component.autoDisplayTooltip = EditorGUILayout.Toggle("Auto display tooltip", component.autoDisplayTooltip);
        EditorGUILayout.PropertyField(alternativeKey, true);
        EditorGUILayout.PropertyField(onMouseEnterProperty, true);
        EditorGUILayout.PropertyField(onMouseExitProperty, true);
        EditorGUILayout.PropertyField(mouseDownAudio, true);
        EditorGUILayout.PropertyField(mouseUpAudio, true);
        EditorGUILayout.PropertyField(mouseEnterAudio, true);
        EditorGUILayout.PropertyField(mouseExitAudio, true);

        EditorGUILayout.PropertyField(changeFontColor, true);
        EditorGUILayout.PropertyField(defaultFontColor, true);
        EditorGUILayout.PropertyField(highlightFontColor, true);
        EditorGUILayout.PropertyField(disabledFontColor, true);

        serializedObject.ApplyModifiedProperties();

        if (GUI.changed && !EditorApplication.isPlaying)
        {
            EditorUtility.SetDirty(component);
            EditorSceneManager.MarkSceneDirty(component.gameObject.scene);
        }
    }
}
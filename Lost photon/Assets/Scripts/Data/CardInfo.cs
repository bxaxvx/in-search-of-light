﻿using UnityEngine;

[CreateAssetMenu(fileName = "Card", menuName = "Card")]
public class CardInfo : ScriptableObject
{
    [Header("Basic Info")]
    public ECardType cardType;
    public Sprite cardSprite;
    public string placeholderName;
    public int autoPickedUpAtLevel;
    public bool isConsumed;
    public AudioClip onUseSound;

    [Header("Disassembling")]
    public ECardType disassembledIntoA;
    public ECardType disassembledIntoB;
}

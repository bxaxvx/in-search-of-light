﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightController : MonoBehaviour
{
    public Robot GoodRobot;
    public Robot BadRobot;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void StartFight()
    {
        GoodRobot.Reset();
        BadRobot.Reset();
        GoodRobot.StartFightWithEnemy(BadRobot);
        BadRobot.StartFightWithEnemy(GoodRobot);
    }

}

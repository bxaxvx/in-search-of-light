﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlLocation : Location
{
    public AudioClip hurraySound;
    public GameObject heartEmoji;

    private bool gotLove = false;

    protected override void OnApplyCard(CardDisplay card)
    {
        base.OnApplyCard(card);
        bool isUsed = true;

        switch (card.CardInfo.cardType)
        {
            case ECardType.Flowers:
                {
                    if (gotLove)
                    {
                        isUsed = false;
                        break;
                    }
                    else
                    {
                        gotLove = true;
                        GameManager.Instance.AddCard(ECardType.Berries);
                        GameManager.Instance.PlaySoundFX(hurraySound, 1f, 1.4f);
                        break;
                    }
                }

            default:
                if (!gotLove)
                {
                    DelayAction(0.1f, () => heartEmoji.SetActive(true));
                    DelayAction(1.6f, () => heartEmoji.SetActive(false));
                    isUsed = false;
                    break;
                }
                else
                {
                    isUsed = false;
                    break;
                }
        }
        if (isUsed && card.CardInfo.onUseSound != null)
            GameManager.Instance.PlaySoundFX(card.CardInfo.onUseSound);
        if (card.CardInfo.isConsumed && isUsed)
            card.StartDestroying();
    }
}

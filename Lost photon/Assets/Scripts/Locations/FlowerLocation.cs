﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerLocation : Location
{
    private bool isFlowersCut;
    [SerializeField]
    private Sprite usedLocationSprite;
    protected override void OnApplyCard(CardDisplay card)
    {
        base.OnApplyCard(card);
        bool isUsed = true;
        switch (card.CardInfo.cardType)
        {
            case ECardType.Secateurs:
                {
                    if (isFlowersCut)
                    {
                        isUsed = false;
                        break;
                    }
                    else
                    {
                        isFlowersCut = true;
                        backgroundRenderer.sprite = usedLocationSprite;
                        GameManager.Instance.AddCard(ECardType.Flowers);
                        break;
                    }
                }

            default: isUsed = false; break;
        }
        if (isUsed && card.CardInfo.onUseSound != null)
            GameManager.Instance.PlaySoundFX(card.CardInfo.onUseSound);
        if (card.CardInfo.isConsumed && isUsed)
            card.StartDestroying();
    }
}

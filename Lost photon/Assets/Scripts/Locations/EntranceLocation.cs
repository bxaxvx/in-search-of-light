﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntranceLocation : Location
{
    private bool _gaveWire = false;
    [SerializeField]
    private Sprite usedLocationSprite;

    protected override void OnApplyCard(CardDisplay card)
    {
        base.OnApplyCard(card);
        bool isUsed = true;
        switch (card.CardInfo.cardType)
        {
            case ECardType.Coin:
                {
                    backgroundRenderer.sprite = usedLocationSprite;
                    GameManager.Instance.AddCard(ECardType.Toy);
                    break;
                }
            case ECardType.Secateurs:
                if (!_gaveWire)
                {
                    GameManager.Instance.AddCard(ECardType.Wire);
                    _gaveWire = true;
                }
                break;
            default: isUsed = false; break;
        }
        if (card.CardInfo.isConsumed && isUsed)
            card.StartDestroying();
    }
}

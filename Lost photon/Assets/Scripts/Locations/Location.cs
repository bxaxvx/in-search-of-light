using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Location : MonoBehaviour
{
    private const float HOVER_SCALE_FACTOR = 1.02f;

    [SerializeField]
    protected SpriteRenderer backgroundRenderer;
    [SerializeField]
    protected Collider2D collider2d;
    public Transform characterLocation;

    public static bool isPointerOverLocation;

    protected virtual void OnApplyCard(CardDisplay card)
    {
        GameManager.Instance.mainCharacter.transform.SetParent(characterLocation, false);
        GameManager.Instance.SelectCard(null);
    }
    protected virtual void OnStartCardHover(CardDisplay card)
    {
        isPointerOverLocation = true;
        isHovered = true;
        transform.localScale = new Vector3(HOVER_SCALE_FACTOR, HOVER_SCALE_FACTOR, 1f);
    }

    protected virtual void OnFinishCardHover(CardDisplay card)
    {
        isPointerOverLocation = false;
        isHovered = false;
        transform.localScale = Vector3.one;
    }

    public virtual void OnPlayerEnter(bool isFirstEnter)
    {

    }

    protected void DelayAction(float time, UnityAction action)
    {
        StartCoroutine(DelayActionCoroutine(time, action));
    }

    protected IEnumerator DelayActionCoroutine(float time, UnityAction action)
    {
        yield return new WaitForSeconds(time);
        if (action != null)
            action();

    }

    private bool isHovered;

    //public virtual bool CardIsReadyToAccept(CardBase card) { return card != null; }
    private void Update()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var selectedCard = GameManager.Instance.SelectedCard;
        if (Input.GetMouseButton(0))
        {
            if (!isHovered)
            {
                if (GameManager.Instance.IsDraggingCard && collider2d.OverlapPoint(mousePosition))
                    OnStartCardHover(selectedCard);
            }
            else if (!collider2d.OverlapPoint(mousePosition) || !GameManager.Instance.IsDraggingCard)
                OnFinishCardHover(selectedCard);
        }
        else if (isHovered)
            OnFinishCardHover(selectedCard);
        if (Input.GetMouseButtonUp(0) && GameManager.Instance.IsDraggingCard)
        {
            if (collider2d.OverlapPoint(mousePosition))
            {
                OnApplyCard(selectedCard);
            }
        }
    }
}
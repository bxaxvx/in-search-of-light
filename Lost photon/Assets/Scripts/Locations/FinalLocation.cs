﻿using UnityEngine;
using System.Collections;

public class FinalLocation : Location
{
    public Sprite bgWithLight;
    public Sprite bgWithoutLight;
    public AudioClip lampDimmingSound;
    public GameObject Cretids;
    public CutScene FinalCutscene;

    private bool foundGear;

    public override void OnPlayerEnter(bool isFirstEnter)
    {
        base.OnPlayerEnter(isFirstEnter);
        if (isFirstEnter)
        {
            StartCoroutine(LightAnimation());
            isLightOn = false;
        }
        backgroundRenderer.sprite = isLightOn ? bgWithLight : bgWithoutLight;
        backgroundRenderer.enabled = false;
    }

    private bool isLightOn = false;

    private IEnumerator LightAnimation()
    {
        backgroundRenderer.sprite = bgWithoutLight;
        isLightOn = false;
        yield return new WaitForSeconds(16f);
        FinalCutscene.gameObject.SetActive(true);
        backgroundRenderer.enabled = true;
        while (FinalCutscene.gameObject.activeSelf)
        {
            yield return new WaitForSeconds(0.1f);
        }
        GameManager.Instance.PlaySoundFX(lampDimmingSound);
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.8f);
            SwitchLight();
        }
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.4f);
            SwitchLight();
        }
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(0.2f);
            SwitchLight();
        }
        for (int i = 0; i < 7; i++)
        {
            yield return new WaitForSeconds(0.1f);
            SwitchLight();
        }
        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(0.05f);
            SwitchLight();
        }
        backgroundRenderer.sprite = bgWithLight;
        isLightOn = true;
        yield return new WaitForSeconds(3f);
        Cretids.SetActive(true);
    }

    private void SwitchLight()
    {
        isLightOn = !isLightOn;
        backgroundRenderer.sprite = isLightOn ? bgWithLight : bgWithoutLight;
    }

    public void Quit()
    {
        Application.Quit();
    }
}

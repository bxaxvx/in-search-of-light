﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationLocation : Location
{
    protected override void OnApplyCard(CardDisplay card)
    {
        base.OnApplyCard(card);
        bool isUsed = true;
        switch (card.CardInfo.cardType)
        {
            case ECardType.Chain:
                GameManager.Instance._currentLevel.nextLevel.IsLocked = false;
                break;
            default: isUsed = false; break;
        }
        if (isUsed && card.CardInfo.onUseSound != null)
            GameManager.Instance.PlaySoundFX(card.CardInfo.onUseSound);
        if (card.CardInfo.isConsumed && isUsed)
            card.StartDestroying();
    }
}

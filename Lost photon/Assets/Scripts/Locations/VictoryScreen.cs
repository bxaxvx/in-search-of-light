﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScreen : Location
{
    [SerializeField]
    private AudioClip lightsSound;
    [SerializeField]
    private Sprite[] sprites;

    public override void OnPlayerEnter(bool isFirstEnter)
    {
        StartVictoryAnimation();
    }

    public void StartVictoryAnimation()
    {
        backgroundRenderer.sprite = sprites[0];
        gameObject.SetActive(true);
        StartCoroutine(AnimationCoroutine());
        GameManager.Instance.HideUI();
    }

    private IEnumerator AnimationCoroutine()
    {
        GameManager.Instance.musicSource.Stop();
        yield return new WaitForSeconds(4f);
        GameManager.Instance.PlaySoundFX(lightsSound, 1f, true);
        backgroundRenderer.sprite = sprites[1];
        yield return new WaitForSeconds(3f);
        GameManager.Instance.PlaySoundFX(lightsSound, 1f, true);
        backgroundRenderer.sprite = sprites[2];
        yield return new WaitForSeconds(3f);
        GameManager.Instance.PlaySoundFX(lightsSound, 1f, true);
        backgroundRenderer.sprite = sprites[3];
        yield return new WaitForSeconds(3f);
        GameManager.Instance.PlaySoundFX(lightsSound, 1f, true);
        backgroundRenderer.sprite = sprites[4];
        yield return new WaitForSeconds(3f);
        GameManager.Instance.musicSource.Play();
        gameObject.SetActive(false);
    }
}

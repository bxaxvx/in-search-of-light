﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLocation : Location
{
    public GameObject lolEmoji;
    public GameObject compoteEmoji;
    public AudioClip kekSound;

    private bool gotCompote = false;

    protected override void OnApplyCard(CardDisplay card)
    {
        base.OnApplyCard(card);
        bool isUsed = true;
        switch (card.CardInfo.cardType)
        {
            case ECardType.ContainerWithWaterAndFruits:
                {
                    GameManager.Instance.AddCard(ECardType.Compote);
                    break;
                }
            case ECardType.Coin:
                isUsed = false;
                GameManager.Instance.PlaySoundFX(kekSound);
                DelayAction(0.1f, () => lolEmoji.SetActive(true)); 
                DelayAction(1.6f, () => lolEmoji.SetActive(false));
                break;
            case ECardType.Compote:
                {
                    if (gotCompote)
                    {
                        isUsed = false;
                        break;
                    }
                    else
                    {
                        gotCompote = true;
                        GameManager.Instance.AddCard(ECardType.Chain);
                        break;
                    }

                }
            default:
                {
                    if (!gotCompote)
                    {
                        DelayAction(0.1f, () => compoteEmoji.SetActive(true));
                        DelayAction(1.6f, () => compoteEmoji.SetActive(false));
                        isUsed = false;
                        break;
                    }
                    else
                    {
                        isUsed = false;
                        break;
                    }
                }

        }
        if (isUsed && card.CardInfo.onUseSound != null)
            GameManager.Instance.PlaySoundFX(card.CardInfo.onUseSound);
        if (card.CardInfo.isConsumed && isUsed)
            card.StartDestroying();
    }
}

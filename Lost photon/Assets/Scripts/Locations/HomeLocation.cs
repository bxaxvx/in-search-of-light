﻿using UnityEngine;
using System.Collections;

public class HomeLocation : Location
{
    public Sprite bgWithLight;
    public Sprite bgWithoutLight;
    public AudioClip lampDimmingSound;

    private bool foundGear;

    protected override void OnApplyCard(CardDisplay card) 
    {
        base.OnApplyCard(card);
        bool isUsed = true;
        switch (card.CardInfo.cardType)
        {
            case ECardType.Key:
                {
                    GameManager.Instance.AddCard(ECardType.Handle);
                    GameManager.Instance.AddCard(ECardType.Coin);
                    break;
                }
            case ECardType.Container:
                {
                    GameManager.Instance.AddCard(ECardType.ContainerWithWater);
                    break;
                }
            case ECardType.LinemansPliers:
                {
                    if(foundGear)
                    {
                        isUsed = false;
                        break;
                    }
                    else
                    {
                        foundGear = true;
                        GameManager.Instance.AddCard(ECardType.Gear);
                        break;
                    }

                }
            default: isUsed = false; break;
        }
        if (isUsed && card.CardInfo.onUseSound != null)
            GameManager.Instance.PlaySoundFX(card.CardInfo.onUseSound);
        if (card.CardInfo.isConsumed && isUsed)
            card.StartDestroying();
    }

    public override void OnPlayerEnter(bool isFirstEnter)
    {
        base.OnPlayerEnter(isFirstEnter);
        if (isFirstEnter)
            StartCoroutine(LightAnimation());
        else
        {
            isLightOn = false;
            backgroundRenderer.sprite = isLightOn ? bgWithLight : bgWithoutLight;
        }

    }

    private bool isLightOn = false;

    private IEnumerator LightAnimation()
    {
        var titleScreenActive = GameObject.Find("Title").GetComponent<UnityEngine.UI.Image>();
        while (titleScreenActive.enabled)
        {
            yield return new WaitForSeconds(0.1f);
        }
        titleScreenActive.gameObject.SetActive(false);
        backgroundRenderer.sprite = bgWithLight;
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(0.05f);
            SwitchLight();
        }
        GameManager.Instance.PlaySoundFX(lampDimmingSound);
        for (int i = 0; i < 7; i++)
        {
            yield return new WaitForSeconds(0.1f);
            SwitchLight();
        }
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(0.2f);
            SwitchLight();
        }
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.4f);
            SwitchLight();
        }
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.8f);
            SwitchLight();
        }
        GameManager.Instance._currentLevel.nextLevel.IsLocked = false;
    }

    private void SwitchLight()
    {
        isLightOn = !isLightOn;
        backgroundRenderer.sprite = isLightOn ?  bgWithLight : bgWithoutLight;
    }
}

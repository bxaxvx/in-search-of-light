﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Robot : MonoBehaviour
{
    public enum RobotStateEnum
    {
        StateGood,
        StateBroken
    }

    public enum RobotSystems
    {
        None,
        Microscheme,
        Battery,
        Gear
    }

    public int RobotHitDamage;
    public float RobotHPRegenPerSec;
    public float CheckAttackDelay;
    public RobotRecoveryController RecoverUI;
    public Image HPProgressBar;
    public List<int> AttackSequence;

    public AudioClip ImactSound;
    public AudioClip HahaSound;
    
    public List<string> RandomAttackTriggers;
    public List<RobotSystems> RandomAttackSystems;

    private RobotStateEnum _state = RobotStateEnum.StateGood;
    private HashSet<RobotSystems> _robotSystems = new HashSet<RobotSystems>();
    private RobotSystems _recoveryDetail = RobotSystems.None;
    private float _recoveryTime;
    private Robot _enemy;
    private int _robotHp;
    private int _attackSequenceIndex;

    private IEnumerator _attackCorroutine;
    private Animator _animator;

    private List<RobotSystems> DamageStack = new List<RobotSystems>();
    private bool _gaveCoil = false;

    public RobotStateEnum State
    {
        get { return _state; }
    }

    // Start is called before the first frame update

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (HPProgressBar)
        {
            float progress = _robotHp == 0 ? 0.0f : (float)_robotHp / 100;
            HPProgressBar.fillAmount = progress;
        }
        if ((RobotHitDamage > 0) && (Time.frameCount % 10 == 0) && (_robotHp != 0))
        {
            _robotHp = (int)((float)_robotHp + (RobotHPRegenPerSec * Time.fixedDeltaTime * 10));
            _robotHp = Mathf.Min(_robotHp, 100);
        }
        
        if (RecoverUI)
        {
            bool needRepear = (_state != RobotStateEnum.StateGood);
            RecoverUI.UpdateRepearState(needRepear);
        }
    }

    public void Reset()
    {
        _state = RobotStateEnum.StateGood;
        _robotSystems = new HashSet<RobotSystems>()
        {
            RobotSystems.Microscheme,
            RobotSystems.Gear,
            RobotSystems.Battery
        };
        SetRobotRecoverDetail(RobotSystems.None);
        _enemy = null;
        _robotHp = 100;
        _attackCorroutine = null;
        StopAllCoroutines();
        DamageStack = new List<RobotSystems>();
        if (RecoverUI)
        {
            RecoverUI.gameObject.SetActive(true);
            RecoverUI.ResetState();
        }
        ResetAttackSequence();
        Canvas childCanavs = GetComponentInChildren<Canvas>();
        if (childCanavs != null)
        {
            childCanavs.gameObject.SetActive(HPProgressBar != null);
        }
        GetComponent<Animator>().SetBool("Active", true);
    }

    public void ResetAttackSequence()
    {
        _attackSequenceIndex = 0;
    }

    //calling from idle animation
    public IEnumerator AttackChecker()
    {
        while (true)
        {
            yield return new WaitForSeconds(CheckAttackDelay);
            if (_enemy && ((Time.time - _enemy._recoveryTime) < CheckAttackDelay))
            {
                continue;
            }
            if (_enemy &&
                (_robotHp > 0) &&
                (_state == RobotStateEnum.StateGood) &&
                (_enemy._state == RobotStateEnum.StateGood))
            {
                AttackEnemy();
            }
        }
    }

    //calling from hit animation
    public void MakeImpactToEnemy()
    {
        if (_enemy)
        {
            _enemy._animator.SetTrigger("Impact");
            _enemy.ProcessDamagesIfAny();
            GameManager.Instance.PlaySoundFX(ImactSound);
        }
    }

    public void StartFightWithEnemy(Robot enemy)
    {
        _enemy = enemy;
        _attackCorroutine = AttackChecker();
        StartCoroutine(_attackCorroutine);
    }

    private void AttackEnemy()
    {
        int attackIndex = Random.Range(0, RandomAttackTriggers.Count);
        if (AttackSequence.Count > 0)
        {
            attackIndex = AttackSequence[_attackSequenceIndex] - 1;
            ++_attackSequenceIndex;
            if (_attackSequenceIndex > RandomAttackTriggers.Count)
            {
                _attackSequenceIndex = 0;
            }
        }

        string tirgger = RandomAttackTriggers[attackIndex];
        GetComponent<Animator>().SetTrigger(tirgger);
        _enemy.DamageStack.Add(RandomAttackSystems[attackIndex]);
    }

    public void ProcessDamagesIfAny()
    {
        foreach (var damageSystem in DamageStack)
        {
            MakeDamageToSystem(damageSystem);
        }
        DamageStack.Clear();
    }

    private void MakeDamageToSystem(RobotSystems system)
    {
        _robotSystems.Remove(system);

        if (_recoveryDetail == system)
        {
            _robotSystems.Add(_recoveryDetail);

            // if we are NPC - then we not removing detail
            if (RobotHitDamage == 0)
            {
                if (RecoverUI)
                {
                    RecoverUI.RecoverSystem(_recoveryDetail);
                }
                _recoveryDetail = RobotSystems.None;
            }
        } else
        {
            if (RecoverUI)
            {
                RecoverUI.BreakSystem(system);
            }
        }

        if (RobotHitDamage > 0)
        {
            _robotHp -= RobotHitDamage;
            _robotHp = Mathf.Max(0, _robotHp);
            if (_robotHp == 0)
            {
                Die();
            }
        } else
        {
            UpdateRobotState();
        }
    }

    private void Die()
    {
        _state = RobotStateEnum.StateBroken;
        _animator.SetBool("Active", false);
        if (!_gaveCoil)
        {
            GameManager.Instance.AddCard(ECardType.Coil);
            GameManager.Instance.PlaySoundFX(HahaSound);
            _gaveCoil = true;
        }
        
    }

    public bool HaveRecoveryDetail
    {
        get { return _recoveryDetail != RobotSystems.None; }
    }

    public bool SetRobotRecoverDetail(RobotSystems detail)
    {
        if (_state == RobotStateEnum.StateBroken)
        {
            if (!_robotSystems.Contains(detail))
            {
                _robotSystems.Add(detail);
                UpdateRobotState();
                if (RecoverUI)
                {
                    RecoverUI.RecoverSystem(detail);
                    RecoverUI.SetRecoveryDetail(RobotSystems.None);
                }
                _recoveryDetail = RobotSystems.None;
                return true;
            }
            return false;
        } else if (_recoveryDetail == RobotSystems.None)
        {
            if (RecoverUI)
            {
                RecoverUI.SetRecoveryDetail(detail);
            }
            _recoveryDetail = detail;
            _recoveryTime = Time.time;
        }
        return true;
    }

    private void UpdateRobotState()
    {
        bool allOk =
            _robotSystems.Contains(RobotSystems.Microscheme) &&
            _robotSystems.Contains(RobotSystems.Battery) &&
            _robotSystems.Contains(RobotSystems.Gear);

        if (allOk)
        {
            if ((_state == RobotStateEnum.StateBroken) && _enemy)
            {
                _enemy.ResetAttackSequence();
            }
            _state = RobotStateEnum.StateGood;
            _animator.SetBool("Active", true);
        } else
        {
            _state = RobotStateEnum.StateBroken;
            _animator.SetBool("Active", false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CraftSlot : MonoBehaviour
{
    public void OnDragEnd(CardDisplay card)
    {
        GameManager.Instance.SelectCard(null);
        GameManager.Instance.craftManager.OnCardAddedToSlot();
    }
}

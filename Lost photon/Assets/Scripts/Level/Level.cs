﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Level : MonoBehaviour
{
    public int levelNumber;
    public Level previousLevel;
    public Level nextLevel;
    public CutScene cutScene;

    [SerializeField]
    private List<Location> locations;
    [SerializeField]
    private bool isLocked;

    public bool IsLocked
    {
        get { return isLocked; }
        set
        {
            isLocked = value;
            GameManager.Instance.UpdateButtons();
        }
    }

    private bool wasVisitedBefore;

    public void EnterLevel(UnityAction<int> onFirstEnterCallback)
    {
        if (!wasVisitedBefore)
            onFirstEnterCallback(levelNumber);
        GameManager.Instance.mainCharacter.transform.SetParent(locations[0].characterLocation, false);
        if ((cutScene == null) || wasVisitedBefore)
        {
            gameObject.SetActive(true);
        }
        foreach (var location in locations)
            location.OnPlayerEnter(!wasVisitedBefore);
        wasVisitedBefore = true;
    }

    public void LeaveLevel()
    {
        gameObject.SetActive(false);
    }
}

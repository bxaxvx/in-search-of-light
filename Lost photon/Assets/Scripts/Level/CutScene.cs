﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutScene : MonoBehaviour
{
    public AudioClip StartSound;
    public AudioClip EndSound;
    public Canvas CanvasToDisable;
    public Level NextLevel;
    
    private UnityAction _endAction;

    public void PlayAudio(AudioClip clip)
    {
        GameManager.Instance.PlaySoundFX(clip);
    }

    public void StartScene(UnityAction finishCb)
    {
        gameObject.SetActive(true);
        _endAction = finishCb;
        CanvasToDisable.gameObject.SetActive(false);
    }

    public void EnableLevel()
    {
        if (NextLevel)
        {
            NextLevel.gameObject.SetActive(true);
        }
    }

    public void FinishScene()
    {
        CanvasToDisable.gameObject.SetActive(true);
        if (_endAction != null)
        {
            _endAction();
        }
        gameObject.SetActive(false);
    }
}
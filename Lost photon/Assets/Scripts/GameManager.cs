﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using System.Collections.Generic;
using UnityEditor;
using System;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;

    public CraftManager craftManager;
    public GamingCardsPanel GamingCardsPanel;

    [SerializeField]
    private Level _startLevel;
    [SerializeField]
    public AudioSource musicSource;
    [SerializeField]
    private ExtendedButton moveLeftButton;
    [SerializeField]
    private List<CraftingRecipe> craftingRecipes;
    public GameObject mainCharacter;

    public ECardType GetCraftResult(CardInfo cardInfo1, CardInfo cardInfo2)
    {
        foreach (var recipe in craftingRecipes)
            if ((cardInfo1.cardType == recipe.cardA && cardInfo2.cardType == recipe.cardB) || (cardInfo1.cardType == recipe.cardB && cardInfo2.cardType == recipe.cardA))
            {
                return recipe.resultCard;
            }
        return ECardType.None;
    }

    public CardInfo GetCardInfo(ECardType outputCard)
    {
        foreach (var card in cardsInfo)
        {
            if (card.cardType == outputCard)
            {
                return card;
            }
        }
        return null;
    }

    public ECardType[] GetDisassembleResult(CardInfo cardInfo)
    {
        foreach (var card in cardsInfo)
            if (card.cardType == cardInfo.cardType && card.disassembledIntoA != ECardType.None)
            {
                if (card.disassembledIntoB != ECardType.None)
                    return new ECardType[] { card.disassembledIntoA, card.disassembledIntoB };
                else
                    return new ECardType[] { card.disassembledIntoA };
            }
        return null;
    }

    [SerializeField]
    private ExtendedButton moveRightButton;
    [SerializeField]
    private List<CardInfo> cardsInfo = new List<CardInfo>();
    public Level _currentLevel;
    private CardDisplay _selectedCard;

    private void Awake()
    {
        _instance = this;
        ChangeLevel(_startLevel);
    }

    public static GameManager Instance
    {
        get { return _instance; }
    }

#if UNITY_EDITOR
    [ContextMenu("Refresh data")]
    public void RefreshData()
    {
        cardsInfo.Clear();
        string[] guids = AssetDatabase.FindAssets("t:ScriptableObject", new string[] { "Assets/Data/Cards" });
        foreach (var guid in guids)
            cardsInfo.Add((CardInfo)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(CardInfo)));
        Debug.Log("Refresh data.");
    }
#endif

    private List<CardInfo> GetLevelEnterCards(int levelNumber)
    {
        Assert.IsTrue(levelNumber > 0);
        List<CardInfo> cards = new List<CardInfo>();
        foreach (var card in cardsInfo)
            if (card.autoPickedUpAtLevel == levelNumber)
                cards.Add(card);
        return cards;
    }

    public AudioClip pickupSound;

    private void OnFirstEnter(int levelNumber)
    {
        Action action = () =>
        {
            if (levelNumber == 2)
                musicSource.Play();
            var cardsToAdd = GetLevelEnterCards(levelNumber);
            if (cardsToAdd.Count > 0)
                PlaySoundFX(pickupSound);
            foreach (var card in cardsToAdd)
            {
                GamingCardsPanel.AddCardToSlot(card);
            }
        };

        if (_currentLevel.cutScene)
        {
            _currentLevel.cutScene.StartScene(() =>
            {
                action();
            });
        } else
        {
            action();
        }
    }

    public void AddCard(ECardType cardType)
    {
        foreach (var card in cardsInfo)
        {
            if (card.cardType == cardType)
            {
                GamingCardsPanel.AddCardToSlot(card);
                return;
            }
        }
        Debug.LogError("No card found: " + cardType);
    }

    private void ChangeLevel(Level level)
    {
        if (_currentLevel != null)
            _currentLevel.LeaveLevel();
        _currentLevel = level;
        _currentLevel.EnterLevel(OnFirstEnter);
        UpdateButtons();
    }

    public void UpdateButtons()
    {
        if (_currentLevel.nextLevel != null)
        {
            moveRightButton.gameObject.SetActive(true);
            moveRightButton.interactable = !_currentLevel.nextLevel.IsLocked;
        }
        else
            moveRightButton.gameObject.SetActive(false);
        if (_currentLevel.previousLevel != null)
        {
            moveLeftButton.gameObject.SetActive(true);
            moveLeftButton.interactable = !_currentLevel.previousLevel.IsLocked;
        }
        else
            moveLeftButton.gameObject.SetActive(false);
    }

    public void NextLevel()
    {
        Assert.IsNotNull(_currentLevel);
        Assert.IsNotNull(_currentLevel.nextLevel);
        ChangeLevel(_currentLevel.nextLevel);
    }

    public void PrevLevel()
    {
        Assert.IsNotNull(_currentLevel);
        Assert.IsNotNull(_currentLevel.previousLevel);
        ChangeLevel(_currentLevel.previousLevel);
    }

    public void HideUI()
    {
        GameObject.Find("CardsPanel").SetActive(false);
        GameObject.Find("CardsRoot").SetActive(false);
        GameObject.Find("Controls").SetActive(false);
    }

    //public void ApplyCard()
    //{
    //    _currentLevel.ApplyCard(_selectedCard);
    //    _selectedCard = null;
    //}

    public void SelectCard(CardDisplay card)
    {
       _selectedCard = card;
    }

    public CardDisplay SelectedCard
    {
        get { return _selectedCard; }
    }

    public bool IsDraggingCard
    {
        get { return _selectedCard != null; }
    }


    public void PlaySoundFX(AudioClip clip, float volumeScale = 1f, bool randomizePitch = false)
    {
        var audioSource = Camera.main.GetComponent<AudioSource>();
        audioSource.pitch = randomizePitch ? UnityEngine.Random.Range(0.9f, 1.1f) : 1f;
        audioSource.PlayOneShot(clip, volumeScale);
    }

    public void PlaySoundFX(AudioClip clip, float volumeScale, float pitch)
    {
        var audioSource = Camera.main.GetComponent<AudioSource>();
        audioSource.pitch = pitch;
        audioSource.PlayOneShot(clip, volumeScale);
    }

    //public bool CardReadyToAccept
    //{
    //    get { return _currentLevel.CardIsReadyToAccept(SelectedCard); }
    //}
}

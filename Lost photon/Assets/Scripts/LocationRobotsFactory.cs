﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationRobotsFactory : Location
{
    public FightController FightController;
    public AudioClip ConsumedDetailSound;
    public AudioClip WrongCardSound;
    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnEnable()
    {
        //GameManager.Instance.AddCardToPlayer("GamingCardRobotBattery");
        //GameManager.Instance.AddCardToPlayer("GamingCardRobotGear");
        //GameManager.Instance.AddCardToPlayer("GamingCardRobotMicroscheme");
        FightController.StartFight();
    }

    protected override void OnApplyCard(CardDisplay card)
    {
        base.OnApplyCard(card);
        bool consumed = false;
        switch (card.CardInfo.cardType)
        {
            case ECardType.Microscheme:
                {
                    consumed = FightController.GoodRobot.SetRobotRecoverDetail(Robot.RobotSystems.Microscheme);
                    break;
                }
            case ECardType.Battery:
                {
                    consumed = FightController.GoodRobot.SetRobotRecoverDetail(Robot.RobotSystems.Battery);
                    break;
                }
            case ECardType.Gear:
                {
                    consumed = FightController.GoodRobot.SetRobotRecoverDetail(Robot.RobotSystems.Gear);
                    break;
                }
            case ECardType.GeneratorCoil:
            {
                    GameManager.Instance.NextLevel();
                break;
            }
            default:
                {
                    break;
                }
        }
        if (consumed)
        {
            GameManager.Instance.PlaySoundFX(ConsumedDetailSound);
        } else
        {
            GameManager.Instance.PlaySoundFX(WrongCardSound);
        }
    }
}
